Project: My Library App
=======================

This is a database project to implement a library system as a network web
application.  It is to incorporate multiple database tables with required
relationships to allow for user login, entering new books and videos, checking
out new books and videos, anonymous searching of books and videos, and library
reports.

By: pjansen392 (Peter Jansen)
For: Computer science database class

Setting up local use:
=====================

This rails program requires a working install of rails and a running PostgreSQL
server.  

1. To setup postgres, create a user role myapp with password: P@55w0rd!
2. [postgres]:$ createuser --interactive -P
3. The user doesn't need to be superuser or have the ability to create other
   roles.  

